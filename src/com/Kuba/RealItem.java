package com.Kuba;

public class RealItem extends ListItem {

//    private String name;
    private int xDimention;
    private int yDimention;
    private int zDimention;

    public RealItem(String name, int xDimention, int yDimention, int zDimention) {
        this.name = name;
        this.xDimention = xDimention;
        this.yDimention = yDimention;
        this.zDimention = zDimention;
    }

    public String getName() {
        return name;
    }
}
