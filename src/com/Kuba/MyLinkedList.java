package com.Kuba;

public class MyLinkedList {
    private ListItem head;
    private ListItem currentItem;

    public MyLinkedList() {
    }

    public void addItemToMyLinkedList(ListItem itemToAdd) {
        if (head == null) {
            head = itemToAdd;
        } else {
            this.currentItem = head;
            while (this.currentItem.getNextItem() != null) {
                this.currentItem = this.currentItem.getNextItem();
            }
            this.currentItem.setNextItem(itemToAdd);
            itemToAdd.setPreviousItem(this.currentItem);
        }
    }

    public void removeItemFromMyLinkedList(ListItem itemToRemove) {
        if (itemToRemove != null) {
            ListItem newPrevious = itemToRemove.getPreviousItem();
            ListItem newNext = itemToRemove.getNextItem();
            if (itemToRemove.getNextItem() != null) {
                itemToRemove.getNextItem().setPreviousItem(newPrevious);
            }
            if (itemToRemove.getPreviousItem() != null) {
                itemToRemove.getPreviousItem().setNextItem(newNext);
            }

        }
    }


    public ListItem getHead() {
        return head;
    }
    public void addItemToMyLinkedListInAlphabeticalOrder(ListItem itemToAdd) {
        if (head == null) {
            head = itemToAdd;
        } else {
            this.currentItem = head;
            if (itemToAdd.getName().compareTo(head.getName())<0){
                head.setPreviousItem(itemToAdd);
                itemToAdd.setNextItem(head);
                this.head = itemToAdd;
            }else{
                while (this.currentItem.getNextItem()!= null &&itemToAdd.getName().compareTo(this.currentItem.getNextItem().getName()) > 0) {
                        this.currentItem = this.currentItem.getNextItem();
                    }
                if(this.currentItem.getNextItem() == null) {
                    this.currentItem.setNextItem(itemToAdd);
                    itemToAdd.setPreviousItem(this.currentItem);
                }
                else if (itemToAdd.getName().compareTo(this.currentItem.getNextItem().getName())<0){
                    itemToAdd.setPreviousItem(this.currentItem);
                    itemToAdd.setNextItem(this.currentItem.getNextItem());
                    this.currentItem.getNextItem().setPreviousItem(itemToAdd);
                    this.currentItem.setNextItem(itemToAdd);
                }
            }
        }
    }
}
