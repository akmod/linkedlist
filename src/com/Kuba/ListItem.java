package com.Kuba;

public abstract class ListItem {
    private ListItem previousItem;
    private ListItem nextItem;
    public String name;

//    public ListItem() {
//
//    }

    public ListItem getPreviousItem() {
        return previousItem;
    }

    public void setPreviousItem(ListItem previousItem) {
        this.previousItem = previousItem;
    }

    public ListItem getNextItem() {
        return nextItem;
    }

    public void setNextItem(ListItem nextItem) {
        this.nextItem = nextItem;
    }

    public String getName() {
        return name;
    }
}
